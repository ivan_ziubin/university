﻿using System;
using System.Collections.Generic;
using System.Linq;
using static System.Console;

namespace lab3
{
    class Program
    {
        #region work with cipher
        private static List<char> InitOrdinaryAlphabet(int symbolShift, int alphabetSize)
        {
            var alphabet = new List<char>();
            for (var i = 0; i < alphabetSize; i++)
            {
                alphabet.Add((char)(i + symbolShift));
            }
            return alphabet;
        }

        private static List<char> InitChipeAlphabets(char[] key, List<char> ordinaryAlphabet)
        {
            var alphabet = ordinaryAlphabet.ToList();
            alphabet.RemoveAll(c => ordinaryAlphabet.Any(a => key.Contains(c)));
            alphabet.InsertRange(0, key);
            return alphabet;
        }
        
        private static List<char> YieldCipher(List<char> message, List<char> alphabetTo, List<char> alphabetFrom)
        {
            for (var index = 0; index < message.Count; index++)
            {
                var c = message[index];
                var indexReplacement = alphabetFrom.IndexOf(c);
                if (indexReplacement != -1)
                {
                    message[index] = alphabetTo[indexReplacement];
                }
            }
            return message;
        }

        private static List<char> TransmissionsCipher(List<char> message, int height, int width)
        {
            var encryptorArray = new char[height, width];
            var index = 0;
            var outMessage = new List<char>();
            for (var i = 0; i < height; i++)
            {
                for (var j = 0; j < width; j++)
                {
                    encryptorArray[i, j] = message[index];
                    index++;
                }
            }

            for (var i = 0; i < width; i++)
            {
                for (var j = 0; j < height; j++)
                {
                    outMessage.Add(encryptorArray[j, i]);
                }
            }
            return outMessage;
        }

        #endregion
  
        static void Main(string[] args)
        {
            var key = ReadFromConsole("Enter key phrase for Calling cipher").ToCharArray();
            var (alphabetSize, symbolShift) = LangChoose(key[0]);
            var ordinaryAlphabet = InitOrdinaryAlphabet(symbolShift, alphabetSize);
            WriteLine("\nRegular alphabet:");
            ordinaryAlphabet.ForEach(c => Write(c + " "));
            var chipeAlphabet = InitChipeAlphabets(key, ordinaryAlphabet);
            WriteLine("\nYield cipher alphabet:");
            chipeAlphabet.ForEach(c => Write(c + " "));
            
            var message = ReadFromConsole("\nWrite message that you want to ciphering").ToList();
            
            // yield encryption
            var yieldCiphersMessage = YieldCipher(message, chipeAlphabet, ordinaryAlphabet);
            WriteLine("\nYield encrypted message:");
            yieldCiphersMessage.ForEach(Write);
            
            // transmission encryption
            var transmissionsCipherMessage = TransmissionsCipher(yieldCiphersMessage, 3, 6);
            WriteLine("\nTransmission Encrypted message:");
            transmissionsCipherMessage.ForEach(Write);
            
            // transmission decryption
            var transmissionsDecryptedMessage = TransmissionsCipher(transmissionsCipherMessage, 6, 3);
            WriteLine("\nTransmission Decrypted message:");
            transmissionsDecryptedMessage.ForEach(Write);
            
            // yield decryption
            var fullDecrypted = YieldCipher(transmissionsDecryptedMessage, ordinaryAlphabet, chipeAlphabet);
            WriteLine("\nDecrypted message:");
            fullDecrypted.ForEach(Write);
        }

        #region helper function

        private static string ReadFromConsole(string askPhrase)
        {
            WriteLine(askPhrase);
            var consoleInput = ReadLine()?.ToLower();
            return consoleInput;
        }
        private static (int, int) LangChoose(char firstLetter)
        {
            int charKey = firstLetter;
            return charKey switch
            {
                >= 97 and <= 122 => (26, 97),
                >= 1072 and <= 1103 => (33, 1072),
                _ => throw new ArgumentOutOfRangeException()
            };
        }
        #endregion
    }
}