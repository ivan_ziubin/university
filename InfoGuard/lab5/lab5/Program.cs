﻿using System.IO;
using System.Security.Cryptography;
using static System.Console;

namespace lab5
{
    internal class Program
    {
        public static void Main()
        {
            const string original = "Here is some data to encrypt!";

            // Create a new instance of the AesCryptoServiceProvider
            // class.  This generates a new key and initialization
            // vector (IV).
            using var myAes = new AesCryptoServiceProvider();
            // Encrypt the string to an array of bytes.
            var encrypted = EncryptStringToBytes_Aes(original, myAes.Key, myAes.IV);
            // Decrypt the bytes to a string.
            var roundtrip = DecryptStringFromBytes_Aes(encrypted, myAes.Key, myAes.IV);


            //Display the original data and the decrypted data.
            WriteLine("Original:   {0}", original);
            foreach (var b in encrypted)
            {
                Write(b);
            }
            WriteLine("\nRound Trip: {0}", roundtrip);
        }

        private static byte[] EncryptStringToBytes_Aes(string plainText, byte[] key, byte[] iv)
        {
            // Create an AesCryptoServiceProvider object
            // with the specified key and IV.
            using SymmetricAlgorithm aesAlg = new AesManaged();
            aesAlg.Key = key;
            aesAlg.IV = iv;
            aesAlg.Mode = CipherMode.CFB;
            // Create an encryptor to perform the stream transform.
            var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

            // Create the streams used for encryption.
            using var msEncrypt = new MemoryStream();
            using var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
            using (var swEncrypt = new StreamWriter(csEncrypt))
            {
                //Write all data to the stream.
                swEncrypt.Write(plainText);
            }
            var encrypted = msEncrypt.ToArray();

            // Return the encrypted bytes from the memory stream.
            return encrypted;
        }

        private static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an AesCryptoServiceProvider object
            // with the specified key and IV.
            using SymmetricAlgorithm aesAlg = new AesManaged();
            aesAlg.Key = key;
            aesAlg.IV = iv;
            aesAlg.Mode = CipherMode.CFB;

            // Create a decryptor to perform the stream transform.
            var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

            // Create the streams used for decryption.
            using var msDecrypt = new MemoryStream(cipherText);
            using var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
            using var srDecrypt = new StreamReader(csDecrypt);
            // Read the decrypted bytes from the decrypting stream
            // and place them in a string.
            plaintext = srDecrypt.ReadToEnd();

            return plaintext;
        }
    }
}