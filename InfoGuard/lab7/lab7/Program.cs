﻿using System.Collections.Generic;
using static System.Console;

namespace lab7
{
    class Program
    {
        static void Main(string[] args)
        {
            // ElGamal q = new ElGamal(2147483647);//(20996023);//(7687);
            var q = new ElGamal(7687);
            WriteLine($"P: {q.P}\nA: {q.G}\nOpen: {q.KOpen}");//Close: {q.KClose}\n
            const string message = "Ivan Ziubin";
            WriteLine("Input text: " + message);
            var text = q.Encrypting(message);
            WriteLine("Encrypted...");
            foreach (var t in text)
            {
                Write("{" + t[0] + ", " + t[1] + "}, ");
            }
            WriteLine("Decrypted..."+ElGamal.GetPlainFromCipher(text, q.P, q.G, q.KOpen));
            WriteLine("Decrypted with key...");
            WriteLine(q.Decrypting(text));

        }
    }
}