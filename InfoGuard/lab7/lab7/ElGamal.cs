using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using static System.Console;
using static System.Convert;

namespace lab7
{

    class ElGamal
    {

        public decimal P;

        public decimal G;

        public decimal KOpen;

        private decimal _kClose;

        public ElGamal()
        {
            KeyGen();
        }

        public ElGamal(ulong p)
        {
            P = p; /* (x,p)=1 */
            KeyGen(P);
        }

        private void KeyGen()
        {
            P = CreateBigPrime(10);
            G = TakePrimitiveRoot(P);
            _kClose = 3;
            while (Gcd(_kClose, P) != 1)
            {
                _kClose = CreateBigPrime(10) % (P - 1);
            }
            KOpen = PowMod(G, _kClose, P);
        }

        private void KeyGen(decimal prime)
        {
            G = TakePrimitiveRoot(prime);
            var rand = new Random();
            _kClose = 7;
            while (Gcd(_kClose, prime) != 1)
            {
                _kClose = (rand.Next(1, Int32.MaxValue) * rand.Next(1, Int32.MaxValue)) % (prime - 1);
            }
            KOpen = PowMod(G, _kClose, prime);
        }

        private static decimal Gcd(decimal a, decimal b)
        {
            return b == 0 ? a : Gcd(b, a % b);
        }

        private decimal TakePrimitiveRoot(decimal primeNum)
        {
            for (ulong i = 0; i < primeNum; i++)
                if (IsPrimitiveRoot(primeNum, i))
                    return i;
            return 0;
        }

        private bool IsPrimitiveRoot(decimal p, decimal a)
        {
            if (a is 0 or 1)
                return false;
            decimal last = 1;
            var set = new HashSet<decimal>();
            for (ulong i = 0; i < p - 1; i++)
            {
                last = (last * a) % p;                
                if (set.Contains(last)) // Если повтор
                    return false;
                set.Add(last);
            }
            return true;
        }

        public List<decimal[]> Encrypting(string message)
        {
            var binary = Encoding.UTF8.GetBytes(message);
            var cipherMessage = new List<decimal[]>(); //Хранение шифртекста - пары чисел 
            var rand = new Random();
            var pair = new decimal[2];
            decimal k = 0;
            foreach (var t in binary)
            {
                k = (rand.Next(1, Int16.MaxValue) * rand.Next(1, Int16.MaxValue))%(P-1);
                pair = new decimal[2];
                pair[0] = PowMod(G, k, P);
                pair[1] = (PowMod(KOpen, k, P) * t) % P;
                cipherMessage.Add(pair);
            }
            return cipherMessage;
        }

        public string Decrypting(List<decimal[]> cipherMessage)
        {
            return cipherMessage.Select(t => (byte) (PowMod(
                EuclideanAlgorithm(P, t[0]), _kClose, P) * t[1] % P))
                .Aggregate("", (current, n) =>
                    current + Encoding.ASCII.GetChars(new[] {n})[0]);
        }

        public static string GetPlainFromCipher(List<decimal[]> cipherMessage, decimal p, decimal g, decimal OpenKey)
        {
            var plain = "";
            foreach (var t in cipherMessage)
            {
                var k = MatchingAlgorithm(g, t[0], p);
                var n = (byte)((PowMod(EuclideanAlgorithm(p, OpenKey), k, p) * t[1]) % p);
                WriteLine($"{t[0]} = {g}^k mod {p}\nk = {k}");
                WriteLine($"M = {n} = (({OpenKey})^-1)^{k} * {t[1]} mod {p}");
                plain += Encoding.ASCII.GetChars(new[] { n })[0];
            }
            return plain;
        }

        private static decimal EuclideanAlgorithm(decimal module, decimal element)
        {
            decimal inverse =0;
            decimal w1 = 0, w3 = module, r1 = 1, r3 = element; //Инициализация
            var q = Math.Floor((w3 /r3));
            while (r3 != 1)
            {
                var cr1 = r1;
                var cr3 = r3;
                r1 = w1 - r1 * q;
                r3 = w3 - r3 * q;
                w1 = cr1;
                w3 = cr3;
                q = Math.Floor(w3 / r3);
            }

            inverse = r1;
            if (inverse < 0) //Устранение отрицательности 
            {
                inverse += module;
            }
            return inverse;
        }

        private static decimal MatchingAlgorithm(decimal a, decimal b, decimal p)
        {
            decimal x=0,
                H = (long)Math.Sqrt(Decimal.ToUInt64(p)) + 1;
            var c = PowMod(a, H, p);
            List<decimal> table_0 = new(),
                table_1 = new();
            table_1.Add((b % p));
            for (long i = 1; i <= H; i++)
            {
                table_0.Add(PowMod(c, i, p));
                table_1.Add(((PowMod(a, i, p) * b) % p));
            }
            decimal q;
            for (short i = 0; i < table_1.Count; i++)
            {
                q = table_0.IndexOf(table_1[i]);
                if (q <= 0) continue;
                x = ((q+1) * H - i);//% (p - 1);
                break;
            }
            return x;
        }

        public static decimal RhoPolard(decimal a, decimal b, decimal p)
        {
            List<decimal> u = new(),
                v = new(),
                z = new();
            var ii = new List<int>();
            decimal x = 0;
            int i2;
            u.Add(0);
            v.Add(0);
            z.Add(1);
            var i = 0;
            while (true)
            {
                if (z[i] > 0 && z[i] < p / 3)
                {
                    u.Add((u[i] + 1) % (p - 1));
                    v.Add(v[i] % (p - 1));
                }
                if (z[i] > p / 3 && z[i] < 2 * (p / 3))
                {
                    u.Add((2 * u[i]) % (p - 1));
                    v.Add((2 * v[i]) % (p - 1));
                }
                if (z[i] > 2 * (p / 3) && z[i] < p)
                {
                    u.Add(u[i] % (p - 1));
                    v.Add((v[i] + 1) % (p - 1));
                }
                z.Add((PowMod(b, u[u.Count - 1], p - 1) * PowMod(a, v[v.Count - 1], p - 1)) % (p - 1));
                i++;
                if (z[i] > 0 && z[i] < p / 3)
                {
                    z[i] = (b * z[i]) % p;
                }
                if (z[i] > p / 3 && z[i] < 2 * (p / 3))
                {
                    z[i] = (z[i] * z[i]) % p;
                }
                if (z[i] > 2 * (p / 3) && z[i] < p)
                {
                    z[i] = (a * z[i]) % p;
                }
                i2 = (int)Math.Ceiling((double)i / 2);
                if (i < 2 || z[i2] != z[i]) continue;
                ii.Add(i2);
                if (Gcd(u[i] - u[i2], p - 1) == 1)
                {
                    x = (EuclideanAlgorithm(p - 1, (u[i] - u[i2])) * (v[i2] - v[i]));
                    x = (x < 0) ? ((p - 1) + x) % (p - 1) : x % (p - 1);
                }
                if (ii.Count >= 3)
                {
                    break;
                }
            }

            if (Gcd(u[i] - u[i2], p - 1) != 1) return x;
            x = EuclideanAlgorithm(p - 1, (u[i] - u[i2]));
            x = (x < 0) ? ((p - 1) + x) % (p - 1) : x % (p - 1);
            return x;
        }

        private ulong CreateBigPrime(short numDec)
        {
            ulong N=1;
            var rand = new Random(DateTime.Now.Millisecond);
            while (Convert.ToString(N).Length < numDec || !IsPrime(N))
            {
                N = (ulong)(rand.Next(0, int.MaxValue) * rand.Next(0, int.MaxValue)) - 1;
            }
            return N;
        }


        private static bool IsPrime(ulong n)
        {
            for (ulong i = 2; i < n / 2 + 1; i++)
            {
                if ((n % i) == 0) return false;
            }
            return true;
        }

        private static decimal PowMod(decimal number, decimal pow, decimal module)
        {
            var q = Convert.ToString((long)pow, 2); //Двоичное представление степени
            BigInteger s = 1, c = (BigInteger)number; //Инициализация
            for (var i = q.Length - 1; i >= 0; i--)
            {
                if (q[i] == '1')
                {
                    s = (s * c) % (BigInteger)module;
                }
                c = (c * c) % (BigInteger)module;
            }
            return (decimal)s;
        }
    }
}