using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    internal sealed class Crypt
    {
        //Размер блока
        private const int BlockSize = 128;

        //Название файла без расширения
        private readonly string _fileName;

        public Crypt(string fileName)
        {
            _fileName = fileName;
        }

        //Бинарное чтение файла
        private char[] ReadFile(string fileExt)
        {
            var chars = new List<char>();
            using (var reader = new BinaryReader(File.Open(_fileName + fileExt, FileMode.Open)))
            {
                //Чтение до конца файла
                while(reader.PeekChar() > -1)
                    chars.Add(reader.ReadChar());
            }
            return chars.ToArray();
        }

        //Бинарная запись файла
        private void WriteFile(string fileExt, char[] chars)
        {
            using var writer = new BinaryWriter(File.Open(_fileName + fileExt, FileMode.OpenOrCreate));
            writer.Write(chars);
        }

        //Разбиваем текст на блоки строк
        private char[][] GetTextBlocks(char[] chars)
        {
            //Блоки должны быть одинаковой длины, поэтому при необходимости дополняем текст
            var tmpChars = chars.ToList();
            while (tmpChars.Count % BlockSize != 0)
                tmpChars.Add('0');

            //На выходе должен получиться массив значений
            chars = tmpChars.ToArray();
            var result = new char[chars.Length / BlockSize][];
            for (var i = 0; i < result.Length; i++)
                result[i] = new char[BlockSize];

            //Заполняем массив
            var iter = 0;

            foreach (var t in result)
            {
                for (var j = 0; j < BlockSize; j++)
                    t[j] = chars[iter++];
                if (iter > chars.Length)
                    break;
            }
            return result;
        }

        //Определяем сдвиг ключа, используя текущий раунд
        private string GetNewKey(string key, int raund)
        {
            //Сдвигаться ключ будет на степень 2-ки, которая определяется раундом
            return key.Aggregate("", (current, elem) => current + elem * (int) Math.Pow(2, raund));
        }

        //Шифрование одного блока
        private char[] CryptBlock(char[] block, string key, bool reverse = false, bool notReverse = false)
        {
            //Определяем блоки r, l
            var blockL = block.Take(BlockSize / 2).ToArray();
            var blockR = block.Skip(BlockSize / 2).ToArray();

            //Меняем блоки местами при первом проходе дешифрования
            if (reverse)
            {
                (blockL, blockR) = (blockR, blockL);
            }

            var temp = new char[BlockSize / 2];
            var iter = 0;
            //Заполняем блоки используя функцию xor по ключу, а также xor с правой частью
            for (var i = 0; i < blockL.Length; i++)
            {
                temp[i] = (char)(blockR[i] ^ (blockL[i] ^ key[iter++]));
                if (iter >= key.Length)
                    iter = 0;
            }  

            //На последнем этапе расшифровки не меняем левый и правый блок
            return notReverse ? blockL.Concat(temp).ToArray() : temp.Concat(blockL).ToArray();
        }

        //Функция шифрования текста
        public void CryptText(string key, int raund, bool decrypt = false)
        {
            //Если включено дешифрование, начинаем с последнего раунда и уменьшаем
            if (decrypt)
            {
                var chars = ReadFile(".enc");
                //Получаем блоки из текста
                var blocks = GetTextBlocks(chars);

                //Выполняем по числу раундов
                for (var i = raund - 1; i >= 0; i--)
                {
                    for (var j = 0; j < blocks.Length; j++)
                    {
                        //Меняет блоки местами перед шифровкой,.. не меняет блоки местами
                        bool p = false, r = false;
                        if (i == raund - 1)
                            p = true;
                        if (i == 0)
                            r = true;

                        blocks[j] = CryptBlock(blocks[j], GetNewKey(key, i), p, r);
                    }
                }

                //Получаем результат
                var result = new char[blocks.Length * BlockSize];
                var iter = 0;

                foreach (var t in blocks)
                    for (var j = 0; j < BlockSize; j++)
                        result[iter++] = t[j];

                WriteFile(".dec",result);
            }
            else
            {
                var chars = ReadFile(".txt");
                //Получаем блоки из текста
                var blocks = GetTextBlocks(chars);

                //Выполняем по числу раундов
                for (var i = 0; i < raund; i++)
                {
                    for (var j = 0; j < blocks.Length; j++)
                        blocks[j] = CryptBlock(blocks[j], GetNewKey(key, i));
                }

                //Получаем результат
                var result = new char[blocks.Length * BlockSize];
                var iter = 0;

                foreach (var t in blocks)
                    for (var j = 0; j < BlockSize; j++)
                        result[iter++] = t[j];

                WriteFile(".enc", result);
            }
        }
    }
}

    