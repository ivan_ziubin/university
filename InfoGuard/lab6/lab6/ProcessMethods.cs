using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace HuffmanTest
{
    public class HuffmanTree
    {
        private List<Node> nodes = new();
        public Node Root { get; set; }
        public readonly Dictionary<char, int> Frequencies = new();

        public void Build(string source)
        {
            foreach (var t in source)
            {
                if (!Frequencies.ContainsKey(t))
                {
                    Frequencies.Add(t, 0);
                }

                Frequencies[t]++;
            }

            foreach (var (key, value) in Frequencies)
            {
                nodes.Add(new Node { Symbol = key, Frequency = value });
            }

            while (nodes.Count > 1)
            {
                var orderedNodes = nodes.OrderBy(node => node.Frequency).ToList<Node>();

                if (orderedNodes.Count >= 2)
                {
                    // Take first two items
                    var taken = orderedNodes.Take(2).ToList<Node>();

                    // Create a parent node by combining the frequencies
                    var parent = new Node()
                    {
                        Symbol = '*',
                        Frequency = taken[0].Frequency + taken[1].Frequency,
                        Left = taken[0],
                        Right = taken[1]
                    };

                    nodes.Remove(taken[0]);
                    nodes.Remove(taken[1]);
                    nodes.Add(parent);
                }

                Root = nodes.FirstOrDefault();

            }

        }

        public BitArray Encode(string source)
        {
            var encodedSource = new List<bool>();

            foreach (var encodedSymbol in source.Select(t => Root.Traverse(t, new List<bool>())))
            {
                encodedSource.AddRange(encodedSymbol);
            }

            var bits = new BitArray(encodedSource.ToArray());

            return bits;
        }

        public string Decode(BitArray bits)
        {
            var current = Root;
            var decoded = "";

            foreach (bool bit in bits)
            {
                if (bit)
                {
                    if (current.Right != null)
                    {
                        current = current.Right;
                    }
                }
                else
                {
                    if (current.Left != null)
                    {
                        current = current.Left;
                    }
                }

                if (!IsLeaf(current)) continue;
                decoded += current.Symbol;
                current = Root;
            }

            return decoded;
        }

        private static bool IsLeaf(Node node)
        {
            return node.Left == null && node.Right == null;
        }

    }
}