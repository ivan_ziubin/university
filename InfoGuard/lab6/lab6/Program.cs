﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using HuffmanTest;

namespace lab6
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = File.ReadAllText("HuffmanNode.txt");
            var huffmanTree = new HuffmanTree();

            // Build the Huffman tree
            huffmanTree.Build(input);

            // Encode
            var encoded = huffmanTree.Encode(input);
            var bytes = new byte[encoded.Length / 8 + ( encoded.Length % 8 == 0 ? 0 : 1 )];
            encoded.CopyTo(bytes, 0 );
            File.WriteAllBytes("img.enc", bytes);
            Console.Write("Encoded: ");

            // Decode
            Console.Write("Decoded: ");

            var decoded = File.ReadAllBytes("img.enc");
            var decodedString = huffmanTree.Decode(new BitArray(decoded));
            File.WriteAllText("img.dec", decodedString);
        }
    }
}