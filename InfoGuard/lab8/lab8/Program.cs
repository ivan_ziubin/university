﻿using System;
using static System.Console;

namespace lab8
{
    class Program
    {
        //Code verification is as follows:
        static void Main(string[] args)
        {
            Int32 data = 0b11000101;
            Int32 ploy = 0b0100;
            ploy <<= 4;
            WriteLine($"The result of the 0th operation:"+Convert.ToString(data, 2));
            for (var i = 0; i <8; i++)
            {
                if ((data & 0b10000000) == 0b10000000)
                {
                    data = (data << 1) ^ ploy;
                }
                else
                {
                    data <<= 1;
                }
                WriteLine($"{i+1}th operation result:"+Convert.ToString(data, 2));
            }
            WriteLine($" Final operation result: "+Convert.ToString(data, 2));          
        }
    }
}