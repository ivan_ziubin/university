﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static System.Console;

namespace lab1
{
    class Program
    {
        private static byte[,] GrayScaleImage(Bitmap image)
        {
            var byteArr = new byte[image.Width, image.Height];
            for (var i = 0; i < image.Width; i++)
            {
                for (var j = 0; j < image.Height; j++)
                {
                    var pixel = image.GetPixel(i, j);
                    var gray = (byte) (.333 * pixel.R + .333 * pixel.G + .333 * pixel.B);
                    byteArr[i, j] = gray;
                }
            }
            return byteArr;
        }
        
        private static Bitmap MedianaFilter(Bitmap bmp, int matrixHeght)
        {
            var byteList = new List<byte>();
            var image = GrayScaleImage(bmp);
            var picOut = new Bitmap(bmp.Width, bmp.Height);
            for (var i = 0; i <= bmp.Width - matrixHeght; i++)
            {
                for (var j = 0; j <= bmp.Height - matrixHeght; j++)
                {
                    for (var x = i; x <= i + (matrixHeght - 1); x++)
                    {
                        for (var y = j; y <= j + (matrixHeght - 1); y++)
                        {
                            byteList.Add(image[x, y]);
                        }
                    }
                    var terms = byteList.ToArray();
                    byteList.Clear();
                    Array.Sort(terms);
                    Array.Reverse(terms);
                    var color = terms[4];
                    picOut.SetPixel(i + 1, j + 1, Color.FromArgb(color, color, color));
                }
            }

            return picOut;
        }

        private static Bitmap Merge(IEnumerable<Bitmap> images, int height, int width, int slice)
        {
            var mergedMap = new Bitmap(width, height);
            var i = 0;
            using var g = Graphics.FromImage(mergedMap);
            foreach (var image in images)
            {
                g.DrawImage(image, new Rectangle(0, slice * i, width, slice));
                i++;
            }
            return mergedMap;
        }
        
        private static void Main()
        {
            const string path = "../../../img/testImg.png";
            const string outPath = "../../../img/outTestimg.png";
            const int threadNumbs = 1; // кол-во потоков тут
            var map = new Bitmap(path, true);
            var stopWatch = new Stopwatch();
            var slice = map.Height / threadNumbs;
            var imageSlices = new List<Bitmap>();

            stopWatch.Start();
            for (var i = 0; i < threadNumbs; i++)
            {
                var cloneRect = new Rectangle(0, slice * i, map.Width, slice);
                var mapClone = map.Clone(cloneRect, map.PixelFormat);
                imageSlices.Add(mapClone);
            }

            var tasochka = Task.Factory.StartNew(() =>
            {
                for (var i = 0; i < threadNumbs; i++)
                {
                    var i2 = i;
                    var proccessing = Task.Factory.StartNew(() =>
                    {
                        imageSlices[i2] = MedianaFilter(imageSlices[i2], 3); // Размер матрицы тут
                    }, TaskCreationOptions.AttachedToParent);
                }
            });
            tasochka.Wait();
            

            var sd12 = Merge(imageSlices, map.Height, map.Width, slice);
            sd12.Save(outPath);
            stopWatch.Stop();
            var ts = stopWatch.Elapsed;
            WriteLine("all done: " + ts);
        }
    }
}