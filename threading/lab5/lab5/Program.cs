﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using static System.Console;

namespace lab5
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("5x1 + 2x2 + x3 = 5");
            WriteLine("x1 + 3x2 - x3 = 2");
            WriteLine("2x1 - x2 + 3x3 = -4\n");
            const double eps = 0.001;
            double x1 = 5;
            double x2 = 2;
            double x3 = -4;
            double temp1 = 0, temp2 = 0, temp3 = 0;
            var stopWatch = new Stopwatch();

            stopWatch.Start();
            do
            {
                Parallel.Invoke(
                    () => SimpleIteration1(ref x1, x2, x3, out temp1),
                    () => SimpleIteration2(x1, ref x2, x3, out temp2),
                    () => SimpleIteration3(x1, x2, ref x3, out temp3));
            } while (Math.Abs(x1 - temp1) >= eps && Math.Abs(x2 - temp2) >= eps && Math.Abs(x3 - temp3) >= eps);
            stopWatch.Stop();
            var ts = stopWatch.Elapsed;
            WriteLine("all done: " + ts);
            WriteLine($"Решение системы:\nx1: {x1}: {temp1},\nx2: {x2}: {temp2},\nx3: {x3}: {temp3}\n" + Math.Abs(x1 - temp1) + " " + Math.Abs(x2 - temp2) + " " + Math.Abs(x3 - temp3));
        }

        static void SimpleIteration1(ref double x1, double x2, double x3, out double temp1)
        {
            temp1 = x1;
            x1 = (5 - 2 * x2 - x3) / 5;
        }

        static void SimpleIteration2(double x1, ref double x2, double x3, out double temp2)
        {
            temp2 = x2;
            x2 = (2 - x1 + x3) / 3;
        }
        
        static void SimpleIteration3(double x1, double x2, ref double x3, out double temp3)
        {
            temp3 = x3;
            x3 = (-4 - 2 * x1 + x2) / 3;
        }
    }
}